const request_barru = require("supertest")("http://barru.pythonanywhere.com"); //url atau endpoint yang dituju
const expect = require("chai").expect; //import library chai untuk validasi

describe("POST User Info", function () { //deskripsikan function untuk test scenario
 
 
 //Test lOGIN
 it("Success Login with valid email and password", async function () { //test case
	 const response = await request_barru //await untuk menunggu request endpoint hingga sukses
	 .post("/login") //tipe http request
	 .send({ email: "riskiamalia@gmail.com", password: "riski" });//data yang dikirim

	 expect(response.body.status).to.eql('SUCCESS_LOGIN');//expect untuk validasi respon
	 expect(response.body.data).to.eql('Welcome riski');
	 expect(response.body.message).to.eql('Anda Berhasil Login');
	 expect(response.body).to.include.keys("data", "message", "status"); 
	 expect(response.statusCode).to.eql(200);
 });
 
 it("Failed Login with empty email", async function () { //test case
	 const response = await request_barru //await untuk menunggu request endpoint hingga sukses
	 .post("/login") //tipe http request
	 .send({ email: "", password: "riski" });//data yang dikirim

	 expect(response.body.status).to.eql('FAILED_LOGIN');//expect untuk validasi respon
	 expect(response.body.data).to.eql('Email tidak valid');
	 expect(response.body.message).to.eql('Cek kembali email anda');
	 expect(response.body).to.include.keys("data", "message", "status"); 
	 expect(response.statusCode).to.eql(200);
 });
 
  it("Failed Login with empty password", async function () { //test case
	 const response = await request_barru //await untuk menunggu request endpoint hingga sukses
	 .post("/login") //tipe http request
	 .send({ email: "riskiamalia@gmail.com", password: "" });//data yang dikirim

	 expect(response.body.status).to.eql('FAILED_LOGIN');//expect untuk validasi respon
	 expect(response.body.data).to.eql("User's not found");
	 expect(response.body.message).to.eql('Email atau Password Anda Salah');
	 expect(response.body).to.include.keys("data", "message", "status"); 
	 expect(response.statusCode).to.eql(200);
 });
 
  it("Success Login with Capital letter email", async function () { //test case
	 const response = await request_barru //await untuk menunggu request endpoint hingga sukses
	 .post("/login") //tipe http request
	 .send({ email: "RISKIAMALIA@GMAIL.COM", password: "riski" });//data yang dikirim

	 expect(response.body.status).to.eql('SUCCESS_LOGIN');//expect untuk validasi respon
	 expect(response.body.data).to.eql('Welcome riski');
	 expect(response.body.message).to.eql('Anda Berhasil Login');
	 expect(response.body).to.include.keys("data", "message", "status"); 
	 expect(response.statusCode).to.eql(200);
 });
 
  it("Failed Login with Capital Password", async function () { //test case
	 const response = await request_barru //await untuk menunggu request endpoint hingga sukses
	 .post("/login") //tipe http request
	 .send({ email: "riskiamalia@gmail.com", password: "RISKI" });//data yang dikirim

	 expect(response.body.status).to.eql('SUCCESS_LOGIN');//expect untuk validasi respon
	 expect(response.body.data).to.eql('Welcome riski');
	 expect(response.body.message).to.eql('Anda Berhasil Login');
	 expect(response.body).to.include.keys("data", "message", "status"); 
	 expect(response.statusCode).to.eql(200);
 });
 
   it("Failed Login with Capital Password", async function () { //test case
	 const response = await request_barru //await untuk menunggu request endpoint hingga sukses
	 .post("/login") //tipe http request
	 .send({ email: "riskiamalia@gmail.com", password: "RISKI" });//data yang dikirim

	 expect(response.body.status).to.eql('SUCCESS_LOGIN');//expect untuk validasi respon
	 expect(response.body.data).to.eql('Welcome riski');
	 expect(response.body.message).to.eql('Anda Berhasil Login');
	 expect(response.body).to.include.keys("data", "message", "status"); 
	 expect(response.statusCode).to.eql(200);
 });
 
   it("Gagal login jika Password benar email salah ", async function () { //test case
	 const response = await request_barru //await untuk menunggu request endpoint hingga sukses
	 .post("/login") //tipe http request
	 .send({ email: "riskiamaliaa@gmail.com", password: "RISKI123" });//data yang dikirim

	 expect(response.body.status).to.eql('FAILED_LOGIN');//expect untuk validasi respon
	  expect(response.body.data).to.eql("User's not found");
	 expect(response.body.message).to.eql('Email atau Password Anda Salah');
	 expect(response.body).to.include.keys("data", "message", "status"); 
	 expect(response.statusCode).to.eql(200);
 });
 
   it("Gagal login jika Password benar email salah", async function () { //test case
	 const response = await request_barru //await untuk menunggu request endpoint hingga sukses
	 .post("/login") //tipe http request
	 .send({ email: "riskiamali123a@gmail.com", password: "RISKI" });//data yang dikirim

	 expect(response.body.status).to.eql('FAILED_LOGIN');//expect untuk validasi respon
	 expect(response.body.data).to.eql("User's not found");
	 expect(response.body.message).to.eql('Email atau Password Anda Salah');
	 expect(response.body).to.include.keys("data", "message", "status"); 
	 expect(response.statusCode).to.eql(200);
 });
 
   it("Verify Failed Login with SQLI in password", async function () { 
    const response = await request_url
      .post("/login")
      .send({ email: "barru.kurniawan@gmail.com", 
              password: "SELECT%count%(*)%FROM%Users%WHERE%Username='jebol'%or%1=1%--%'%AND%Password=%'email'"});

    const isi_data = response.body;

    expect(response.body.status).to.eql('FAILED_LOGIN');
    expect(response.body.message).to.eql("Tidak boleh mengandung symbol");
    expect(isi_data).to.include.keys("data", "message", "status"); 
  });

  it("Verify Failed Login with SQLI in email", async function () { 
    const response = await request_url
      .post("/login")
      .send({ email: "SELECT%count%(*)%FROM%Users%WHERE%Username='jebol'%or%1=1%--%'%AND%Password=%'email'", 
              password: "shanatester"});

    const isi_data = response.body;

    expect(response.body.status).to.eql('FAILED_LOGIN');
    expect(response.body.message).to.eql("Cek kembali email anda");
    expect(isi_data).to.include.keys("data", "message", "status"); 
  });

describe("Verify Max Login for User in 1 IP Address", function () { 
  for (i = 0; i<5; i++) {
    let nomer = i;
    it("Tes ke " + [i] + " Failed Login Max in 1 Session", async function () { 
      const response = await request_url
        .post("/login")
        .send({ email: "dicobain@tester.com", password: "sampeblokir", ip_address: "127.107.42.1"});

      const isi_data = response.body;

      if (nomer === 4) {
        // console.log("Tes ke " + nomer + " failed login attemp")
        expect(response.body.status).to.eql('FAILED_LOGIN');
        expect(response.body.data).to.eql('IP Address Anda diblokir');
        expect(isi_data).to.include.keys("data", "message", "status");
      }

      expect(response.body.status).to.eql('FAILED_LOGIN');
      expect(isi_data).to.include.keys("data", "message", "status"); 
    });
  }
});

it("Verify Failed Login with Max Char in Email Field", async function () {
    let max_email = Array.from(Array(55), () => Math.floor(Math.random() * 36).toString(36)).join('');
    const response = await request_url
    .post("/login") 
    .send({ email: max_email + "@gmail.com", password: "aditya.qa" });

    const isi_data = response.body;

    expect(response.body.status).to.eql('FAILED_LOGIN');
    expect(response.body.data).to.eql('Email/Password melebihin maksimal karakter');
    expect(isi_data).to.include.keys("data", "message", "status"); 
  });

  it("Verify Failed Login with Max Char in Password Field", async function () {
    let max_password = Array.from(Array(55), () => Math.floor(Math.random() * 36).toString(36)).join('');
    const response = await request_url
    .post("/login") 
    .send({ email: 'barru.kurniawan@gmail.com', password: max_password });

    const isi_data = response.body;

    expect(response.body.status).to.eql('FAILED_LOGIN');
    expect(response.body.data).to.eql('Email/Password melebihin maksimal karakter');
    expect(isi_data).to.include.keys("data", "message", "status"); 
  });

  it("Verify Failed Login with Method GET", async function () {
    const response = await request_url
    .get("/login") 
    .send({ email: 'barru.kurniawan@gmail.com', password: 'sman60jakarta' });

    expect(response.status).to.eql(405);
  });

  it("Verify Failed Login with Method PUT", async function () {
    let random_text = Math.random().toString(36).substring(7);
    const response = await request_url
    .put("/login") 
    .send({ email: 'barru.kurniawan@gmail.com', password: 'sman60jakarta' });

    expect(response.status).to.eql(405);
  });

  it("Verify Failed Login with Method PATCH", async function () {
    let random_text = Math.random().toString(36).substring(7);
    const response = await request_url
    .patch("/login") 
    .send({ email: 'barru.kurniawan@gmail.com', password: 'sman60jakarta' });

    expect(response.status).to.eql(405);
  });

  it("Verify Failed Login with Form-Data as Body", async function () {
    let random_text = Math.random().toString(36).substring(7);
    const response = await request_url
      .post("/login")
      .type('form')
      .send({ email: 'barru.kurniawan@gmail.com', password: 'sman60jakarta' });

    expect(response.status).to.eql(500);
  });

  it("Verify Failed Login with xx-www-form-urlencoded as Body", async function () {
    let random_text = Math.random().toString(36).substring(7);
    const response = await request_url
      .post("/login")
      .set('content-Type', 'application/x-www-form-urlencoded')
      .send({ email: 'barru.kurniawan@gmail.com', password: 'sman60jakarta' });

    expect(response.status).to.eql(500);
  });

  it("Verify Failed Login with Integer Type in Email", async function () {
    let random_text = Math.random().toString(36).substring(7);
    const response = await request_url
      .post("/login")
      .send({ email: 123456789, password: random_text});

    expect(response.status).to.eql(500);
  });

  it("Verify Failed Login with Integer Type in Password", async function () {
    let random_text = Math.random().toString(36).substring(7);
    const response = await request_url
      .post("/login")
      .send({ email: random_text + "@gmail.com", password: 123456789});

    expect(response.status).to.eql(500);
  });
 
 
 
});